#!/usr/bin/env python
"""
Purge empty directories
<pere.mato@cern.ch>
Version 1.0
"""
#-------------------------------------------------------------------------------
from __future__ import print_function
import os, sys, glob, time
try:
    import argparse
except ImportError:
    import argparse2 as argparse

#---Main program----------------------------------------------------------------
if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('directory', metavar='directory', default=os.getcwd(), help='directory to prune')
  args = parser.parse_args()
  
  dirs = set(glob.glob(os.path.join(args.directory, '*', '*')) + glob.glob(os.path.join(args.directory, 'MCGenerators', '*', '*')))
  for p in dirs:
      if os.listdir(p) == ['share']:
        os.rmdir(os.path.join(p,'share'))
      while p != args.directory :
         if not os.listdir(p):
            print('==== Removing empty directory ', p)
            os.rmdir(p)
         p = os.path.dirname(p)

