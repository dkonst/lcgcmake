#!/usr/bin/env python

import sys
import json

def parsefile(filename):
  with open(filename, 'r') as f:
    return parse(f.read())

def parse(text):
  data = {}
  text = text.split(', ')
  for key, value in [x.split(':',1) for x in text]:
    data.update({key.strip(): value.strip()})
  # deps
  if 'VERSION' in data:
    if 'DEPENDS' not in data:
      tmp = data['VERSION'].split(',')
      data['VERSION'] = tmp[0]
      data['DEPENDS'] = [x for x in tmp[1:] if len(x) != 0]
    else:
      data['DEPENDS'] = [x for x in data['DEPENDS'].split(',') if len(x) != 0]
  if 'GITHASH' in data:
    data['GITHASH'] = data['GITHASH'].replace('"', '')
  return data

if __name__ == "__main__":
  if len(sys.argv) > 1 and '-h' in sys.argv or '--help' in sys.argv:
    print "Usage: {0} [filename |-]".format(sys.argv[0])
    sys.exit(0)
  if len(sys.argv) == 1 or (len(sys.argv) > 1 and sys.argv[1] == "-"):
    data = sys.stdin.read().strip()
  else:
    try:
      with open(sys.argv[1], 'r') as f:
        data = f.read().strip()
    except Exception as e:
      print "ERROR:", str(e)
      sys.exit(1)
  print json.dumps(parse(data))
